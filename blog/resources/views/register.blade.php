<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 Laravel - Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <!-- Bagian kolom pengisian -->
        <label>First name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <!-- Bagian Gender -->
        <label>Gender:</label><br><br>
        <input type="radio" name=""> Male <br>
        <input type="radio" name=""> Female <br>
        <input type="radio" name=""> Others <br><br>
        <!-- Bagian Nationality -->
        <label for="">Nationality:</label><br>
        <select name="" id="">
            <option value="">Indonesian</option>
            <option value="">Malaysian</option>
            <option value="">Singaporean</option>
            <option value="">Australian</option>
        </select><br><br>
        <!-- Bagian Language Spoken -->
        <label for="">Language Spoken:</label><br>
        <input type="checkbox" name="" id=""> Bahasa Indonesia <br>
        <input type="checkbox" name="" id=""> English <br>
        <input type="checkbox" name="" id=""> Other <br><br>
        <!-- Bagian Bio -->
        <label for="">Bio:</label><br>
        <textarea name="" id="" cols="30" rows="10"></textarea><br><br>
        <!-- Bagian Sign Up -->
        <input type="submit" value="Sign Up">
        
    </form>
</body>
</html>